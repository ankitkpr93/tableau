# Coal Terminal (Australia)

**Visualization available at:** 
[Coal Terminal Maintenance Analysis](https://public.tableau.com/profile/ankitkpr93#!/vizhome/CoalTerminalMaintenanceAnalysis_15927915464430/Analysis-S)

**Tableau concepts used:**

- Advanced Table Calculations
- Advnced Calculated Fields
- Data Joins
- Second layer "Moving Average"
- Tooltip control
- Actions using URLs
- Dashboard
- Storyline

## 1. Dataset and Domain Understanding

The dataset which has been used in this report has been taken from (SuperDataScience 2018) which hosts several data sets across various domains. The dataset used is of Bowen coal basin in Queensland, which is one of the largest coal basins in Australia. The screenshot of the dataset has been shown below. The reason as to why this dataset is used in this report is to underline the importance of Business Intelligence in coal industry and how numerous and diverse problems could be solved by incorporating BI into the business process.

Note: This dataset contains data of September month only. Moreover, this dataset contains only reclaimer’s data, even the columns for Stackers and reclaimers signify figures for reclaimers only.

<img src="Images/Dataset_sample.png" width="700">

The dataset consists the actual and nominal capacity of various machines used at the coal fields. The actual tonnage describes the amount of coal in tons that has been processed by a machine while nominal tonnage describes the nominal capacity of coal that a machine can process in an hour. There are three types of machines that are being used at the coal fields.

1.	Stackers (ST): These are the machines which pile up the coal in huge stacks.

<img src="Images/Stackers.png" width="700">
 
2.	Reclaimers (RL): These are the machines used to recover the coal from stacks made by stackers.

<img src="Images/Reclaimers.png" width="700">

3.	Stackers and Reclaimers (SR): These are the specialized machines that performs the tasks of both stacking and reclaiming.

<img src="Images/SR.png" width="700">
 
## 2. Data Dictionary

The attributes of the dataset are described below:

| Attribute | Description |
| ------ | ------ |
| **Datetime** | Timestamp value (depicting hour) |
| **Nominal Capacity (RL1)** | Nominal processing capacity of reclaimer machine 1|	
| **Actual Tonnes (RL1)** | Actual processing done by reclaimer machine 1|
| **Nominal Capacity (RL2)** | Nominal processing capacity of reclaimer machine 2|
| **Actual Tonnes (RL2)** | Actual processing done by reclaimer machine 2|	
| **Nominal Capacity (SR1)** | Nominal processing capacity of stacker-reclaimer machine 1 |
| **Actual Capacity (SR1)** | Actual processing done by stacker-reclaimer machine 1 |
| **Nominal Capacity (SR4)** | Nominal processing capacity of stacker-reclaimer machine 4 |
| **Actual Capacity (SR4)** | Actual processing done by reclaimer machine 4 |
| **Nominal Capacity (SR6)** | Nominal processing capacity of stacker-reclaimer machine 6 |
| **Actual Capacity (SR6)** | Actual processing done by reclaimer machine 6|




## 3. Problem Statement

The problem is to determine the machine which needs maintenance in the upcoming month, at the right time (neither too early nor too late). These machines are operational across 24/7 over 365 days. Each minute of downtime or machine failure is equivalent to loss of millions of dollars; therefore, it is indispensable to identify the right time for maintenance task.

**Rule for maintenance**

A machine requires maintenance when its average idle capacity surpasses 10% over at least one 8-hour period in the past month. The idle capacity of a machine is defined as following:

<img src="Images/Formula.png" width="400">

The task in this further report is to find which of the five machines has surpassed this idle capacity level.

 
## 4. Visualizations and it’s interpretations

First of all, the descriptive charts are prepared to understand the dataset better. Some of the charts are listed below.

a)	Total actual quantity of coal processed on each day of the month (September).

<img src="Images/image_1.png" width="700">

b)	Total actual quantity of coal processed by each machine on each day of the month (September).

<img src="Images/image_2.png" width="700">
 
As specified, SR machines can be used for both stacking and reclaiming, and this dataset contains only reclaiming data, the gap in the curve for SR1 and SR4A signifies that these machines were used for stacking in that period.

c)	Total quantity of coal process in each hour of the day for the whole month.

<img src="Images/image_3.png" width="700">

After understanding the dataset, several steps have been detailed below to understand the whole process of solving a problem with a BI tool.

1.	Plotting separate graph for actual and nominal quantity of coal vs hour of the day.

All the rows at odd position shows the actual quantity of tonnes processed per hour while rows at even position shows the nominal quantity of coal that a particular machine should have processed.

<img src="Images/image_4.png" width="700">
 
2.	Plotting the above graph for just one machine.

<img src="Images/image_5.png" width="700">

3.	Plotting above graph that shows the difference between actual quantity and nominal quantity by hour.

The table calculation functionality of Tableau has been used to plot the graph below.

<img src="Images/image_6.png" width="700">

 
4.	Plotting above graph for all of the machines.

<img src="Images/image_7.png" width="700">

Till this step, we have calculated the numerator part which is needed to calculate the idle capacity.

5.	Converting above graph to percentage.

<img src="Images/image_8.png" width="700">
 
6.	Adding a 10% reference line to above graph

<img src="Images/image_9.png" width="700">


7.	Plotting above graph to positive using calculate fields in Tableau.

In order to get the values positive, a calculate field has been created which multiplies all the previous values with -1.

<img src="Images/image_10.png" width="700">
 
The problem is not solved yet, we have only got to know when the idle capacity is getting surpassed but not over an 8-hour period. We have to convert above graph to reflect idle capacity over an 8-hour period.

8.	Plotting above graph showing average idle capacity over 8-hour period.

To calculate moving average over 8-hour period, a table calculation has to be written like shown below.

<img src="Images/image_11.png" width="700">

<img src="Images/image_12.png" width="700">

9.	Removing spikes from SR1 and SR4A.

The spikes obtained for SR1 and SR4A in the last plot are misleading because these machines were used as stackers during the gap shown in the plot. It is necessary to adjust the calculations in order to make the correct plot.


<img src="Images/image_13.png" width="700">

<img src="Images/image_14.png" width="700">


Using this graph, we can determine which of the machine needs maintenance. For example, we can perceive from the above graph that, RL1 and SR6 had surpassed its idle capacity limit over an 8-hour period in the month of September, therefore, requires maintenance in the upcoming month i.e. October.

These visualizations could really help coal industry to keep track of their coal handling machines and manage their maintenance strictly. These could also help coal industry companies to save millions of dollars from broke down of their machines. Therefore, Business Intelligence could be used to solve business problems across various domains.

<img src="Images/Storyline.png">

## 5. References

SuperDataScience 2018, Download Practice Datasets, SuperDataScience Team,
<https://www.superdatascience.com/pages/tableau-advanced>.
