# Tableau
Click on below link to look at all visualisations:

[**Tableau Profile**](https://public.tableau.com/profile/ankitkpr93)

## Projects

**1.** [**Coal Terminal**](Coal-Terminal) --> [Description](Coal-Terminal/Readme.md)

**2.** [**Grocery Sales Analysis**](Grocery-Sales) --> [Description](Grocery-Sales/Readme.md)
