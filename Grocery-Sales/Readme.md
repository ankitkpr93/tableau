# Grocery Sales (U.S)

**Visualization available at:** 
[Grocery Sales Analysis](https://public.tableau.com/profile/ankitkpr93#!/vizhome/GrocerySalesAnalysisandidentificationofstatesforgrowth/Dashboard1)

**Tableau concepts used:**

- Advanced Table Calculations
- Advnced Calculated Fields
- Data Joins
- Tooltip control
- Dashboard
- Storyline
